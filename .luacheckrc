codes = true
ignore = { '212/self' }
max_line_length = false
max_cyclomatic_complexity = 10

read_globals = {
  -- WoW API functions.
  'C_Map',
  'C_SuperTrack',
  'C_Texture',
  'CLOSE',
  'CloseDropDownMenus',
  'CreateFrame',
  'CreateFromMixins',
  'GameTooltip',
  'GetBuildInfo',
  'IsAltKeyDown',
  'IsShiftKeyDown',
  'MapCanvasDataProviderMixin',
  'MapCanvasPinMixin',
  'Settings',
  'ToggleDropDownMenu',
  'UIDropDownMenu_AddButton',
  'UIDropDownMenu_AddSpace',
  'UIDropDownMenu_CreateInfo',
  'UiMapPoint',
  'UIParent',
  'UnitFactionGroup',
  'WorldMapFrame',
}

globals = {
  -- Cache storage.
  'HandyNotes_NooksAndCranniesCACHE',
  --- Addon Compartment.
  'HandyNotes_NooksAndCrannies_OnAddonCompartmentClick',
}
